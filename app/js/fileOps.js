var TimerApp = angular.module('TimerApp', ['ngRoute','ngSanitize']);
TimerApp.controller('readFile', function($scope, $http, $interval) {
	//versuchen die Buttons aus dem LocalStorage zu laden.

	$scope.buttonList = JSON.parse(localStorage.getItem('buttonList'));
	// wenn der localStorage leer ist dann die Daten vom Server holen
	if($scope.buttonList==null){
		$http.get('./data/buttons.json').success(function(data) {
			$scope.buttonList = data;
		}) // http.get
	}; // if
	
	

	$scope.cdstart = function(m,s) {
		$scope.min=m;
		$scope.sec=s*1;
		if($scope.sec<10) {$scope.sec="0"+$scope.sec;} //if (sec < 10)
		$scope.timer=$scope.min+":"+$scope.sec;
		if(angular.isDefined($scope.cd)) {$interval.cancel($scope.cd)};
		$scope.cd = $interval(function(){CountDown($scope,$interval);},1000); 
	}; //dstart
}); //controller readFile

TimerApp.controller('addButt', function($scope){
	$scope.addNewButton=function(min,sec) {
		this.m=parseInt(min);
		this.s=parseInt(sec);
		if (isNaN(this.m)||isNaN(this.s))
			{$scope.addButtError="Please enter only Numbers!"; }
		else if (this.s>59) { $scope.addButtError="Seconds bigger then 59"; } //elseif
		else {
			if(this.s<10) {this.s = "0"+this.s;}
			$scope.buttonList.knopf.push({"Minuten":this.m, "Sekunden":this.s});
			localStorage.clear();
			localStorage.setItem('buttonList',angular.toJson($scope.buttonList));	
			$scope.min="";
			$scope.sec="";
			$scope.addButtHTML="Button has been added";
			$scope.addButtError="";
		}// else

	}; //addNewButton
}); //addButt

TimerApp.controller('delButt', function($scope){
	$scope.delButton = function(toDelete) {
		$scope.buttonList.knopf.splice(toDelete,+1);
		localStorage.clear();
		localStorage.setItem('buttonList',angular.toJson($scope.buttonList));
	};
}); //controller delButt

//TimerApp.controller('ngBindHTMLC', ['$scope', function ngBindHTMLC($scope){
//
//	window.console.log("mist");
//	if($scope.added==true){ 
//		$scope.addButtHTML="Button has been added!";
//		$scope.added=false;
//	} else {
//		$scope.addButtHTML="!";
//	} //ifelse
//}]);
//angular.module('ngBindHtmlAdd',['ngSanitize']).controller('ngBindHTMLC', ['$scope', function ngBindHTMLC($scope){
//	$scope.addButtHTML="eingefuegt!";
//}]);

TimerApp.config(['$routeProvider',
	function($routeProvider){
		$routeProvider
		.when('/add', {
			templateUrl: 'partials/add.html'
		}).
		when('/delete', {
			templateUrl: 'partials/delete.html'
		}). 
		when('/countdown',{
			templateUrl: 'partials/countdown.html'
		}).
		when('/',{
			templateUrl: 'partials/countdown.html'
		}).
		otherwise({
			redirectTo: 'partials/countdown.html'
		}); 
	} // function(routeProvider)	
]);
